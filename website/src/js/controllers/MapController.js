app.controller("MapController", [ '$scope', function($scope) {

    angular.extend($scope, {
		center: {
            lat: 52.095,
            lng: 21.823,
            zoom: 8
        },
        defaults: {
            scrollWheelZoom: false
        }
    });
}]);
