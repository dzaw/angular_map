<?php

$dbconn = pg_connect("host=localhost port=5432 dbname=pguser user=pguser password=qweasdzxc");
$result = pg_query($dbconn, "SELECT * FROM test");
//var_dump($result);

$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);

while($row = pg_fetch_assoc($result)) {
    $feature = array(
        'id' => $row['id'],
        'type' => 'Feature', 
        'geometry' => array(
            'type' => 'Point',
            'coordinates' => array('-80.8708', '35.2151')
        ),
        # Pass other attribute columns here
        'properties' => array(
            'name' => 'Name', //$row['Name'] etc..
            'description' => 'Description',
            'country' => 'Country',
            'status' => 'Status',
            'date' => 'Date'
            )
        );
    # Add feature arrays to feature collection array
    array_push($geojson['features'], $feature);
}

header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
$conn = NULL;

?>
